#include "bmp_read_write.h"
#include "bmp.h"
#include "file_open_close.h"
#include "image.h"
#include <stdio.h>



// ----- MAIN functions -----


enum read_status read_from_file_bmp( const char* filename, struct image* img ) {
    if (!filename) return READ_NOT_NAME;

    //open File
    FILE* file = NULL;
    if (open( filename, "rb", &file) == OPEN_ERR) return READ_OPEN_ERR;
    if (!file) return READ_OPEN_ERR;   

    // ----- read data bmp File ------   
    
    enum read_status answer = from_bmp(file, img);
    // ----- **** -----

    //close File
    if (close( &file ) == CLOSE_ERR) return READ_CLOSE_ERR;
    return answer;
}

enum write_status write_to_file_bmp(const char* filename,  struct image* img){

     //open File
    FILE* file = NULL;
    if (open( filename, "wb+", &file) == OPEN_ERR) return WRITE_OPEN_ERR;
    if (!file) return WRITE_ERROR;
    
    // ----- write data bmp File -----
    enum write_status answer = to_bmp(file, img);

    //close File
    if (close( &file ) == CLOSE_ERR) return WRITE_CLOSE_ERR;
    return answer;
}

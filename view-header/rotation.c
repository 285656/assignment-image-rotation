#include "rotation.h"
#include "image.h"
#include <stdint.h>
#include <stdlib.h>

typedef struct image rotate (struct image);

static struct image rotate_counterclockwise(struct image);
static struct image rotate_clockwise(struct image);

rotate* const rotates[] = {
    [CLOCKWISE] = rotate_clockwise,
    [COUNTERCLOCKWISE] = rotate_counterclockwise,
};

struct image rotate_img(  struct image image, enum rotation_mode mode){
    return rotates[mode](image);
}


static struct image get_rotate_from_start(struct image);

// ROTATION_1
static struct image rotate_counterclockwise(struct image image) {
    struct image rotate_image = get_rotate_from_start(image);
     
    for(size_t i=0; i<image.height; i++){
        for (size_t j = 0; j <image.width ; ++j) {
            rotate_image.data[(rotate_image.width-1-i)+rotate_image.width*j]= image.data[image.width*i+j];
        }   
    }    
    return rotate_image;
}


//ROTATION_2
static struct image rotate_clockwise(struct image image) {
    struct image rotate_image = get_rotate_from_start(image);
    
    for(size_t i=0; i<image.height; i++){
        for (size_t j = 0; j <image.width ; ++j) {
            rotate_image.data[i + rotate_image.width*j]= image.data[(image.width-1-j)+image.width*i];
        }
    }   
    return rotate_image;
}

static struct image get_rotate_from_start(struct image image) {
    struct image rotate_image={ .height=image.width,
                                .width=image.height, 
                                .data=(struct pixel *)malloc(image.width*image.height*sizeof(struct pixel))
                              };
    return rotate_image;
}

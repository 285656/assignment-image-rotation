#include <stdint.h>
#ifndef ROTATION_H
#define ROTATION_H

enum rotation_mode {
    CLOCKWISE = 0,
    COUNTERCLOCKWISE,
    
};

struct image rotate_img(  struct image image,enum rotation_mode);

#endif /* ROTATION_H */


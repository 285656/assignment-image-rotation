#include "image.h"
#include <stdio.h>
#ifndef CONVERSION_BMP_H
#define CONVERSION_BMP_H


/*  deserializer   */
enum read_status  {
    READ_OK = 0,  
    READ_INVALID_HEADER,
    READ_NOT_NAME,
    READ_OPEN_ERR,
    READ_CLOSE_ERR,
    READ_ERROR
          
    /* коды других ошибок  */
    };
    

    
enum read_status read_from_file_bmp( const char*, struct image*);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_OPEN_ERR,
    WRITE_CLOSE_ERR
    /* коды других ошибок  */
};



enum write_status write_to_file_bmp(const char*, struct image*);


#endif /* CONVERSION_BMP_H */


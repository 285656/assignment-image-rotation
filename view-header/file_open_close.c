#include <stdio.h>
#include "file_open_close.h"


enum open_status open(const char* filename, const char* mode, FILE** file){
    *file = fopen(filename, mode);
     
    if (!*file) {
        return OPEN_ERR;
    }
   
    return OPEN_OK;
}


enum close_status close(FILE** file) {
    if (fclose(*file)) {
        return CLOSE_ERR;
    }
    return CLOSE_OK;
}

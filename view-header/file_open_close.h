#ifndef FILE_OPEN_CLOSE_H
#define FILE_OPEN_CLOSE_H

enum open_status {
    OPEN_OK = 0,
    OPEN_ERR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERR
};

enum open_status open(const char* filename, const char* mode, FILE** file);

enum close_status close(FILE** file);

#endif /* FILE_OPEN_CLOSE_H */


#include "bmp.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>
#include <stdbool.h>

#define BMP_TYPE 0x4d42 //19778
#define SIZE_BYTEMAPINFOHEADER 40
#define BIT_COUNT 24
#define PLANES 1

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

void bmp_header_print( struct bmp_header const* header, FILE* f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
   
}

uint32_t padding(const uint64_t width){
    return width%4;
}

struct bmp_header create_header(struct image const *img){
struct bmp_header bh={0};
bh.bfType = BMP_TYPE;
bh.bfileSize = (img->width*img->height*sizeof(struct pixel))+ sizeof(struct bmp_header);
bh.bfReserved = 0;
bh.bOffBits = sizeof(struct bmp_header);
bh.biSize = SIZE_BYTEMAPINFOHEADER; 
bh.biWidth = img->width;
bh.biHeight = img->height;
bh.biPlanes = PLANES;
bh.biBitCount = BIT_COUNT;
bh.biCompression = 0;
bh.biSizeImage = sizeof(struct pixel)*img->width*img->height;
bh.biXPelsPerMeter = 0;
bh.biYPelsPerMeter = 0;
bh.biClrUsed = 0;
bh.biClrImportant = 0;
return bh;
}


static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static bool write_header( FILE* f, struct bmp_header *const header ) {
    return fwrite( header, sizeof( struct bmp_header ), 1, f );
}



bool valid_header(struct bmp_header* header){
    
    return header->bfType == BMP_TYPE &&
           header->biBitCount == BIT_COUNT && 
           header->biCompression == 0;
}

enum read_status from_bmp( FILE* in, struct image* img ){
     //read header bmp File
    struct bmp_header header;
    if (!read_header( in, &header )) return READ_INVALID_HEADER;
    //fread(header, sizeof( struct bmp_header), 1, file);

    // Valid header
    bool valid_h = valid_header(&header);
    if (!valid_h ) return READ_INVALID_HEADER;
    
    
    struct pixel* data = malloc(sizeof(struct pixel) * (header.biWidth) * (header.biHeight));
    img->height = header.biHeight; img->width = header.biWidth;
    img->data = data;
    uint32_t const pad = padding(img->width);
    
    fseek(in, sizeof(struct bmp_header), SEEK_SET);
    size_t count = 0;
    for(size_t i=0; i < img->height; i++){
        count += fread(&data[i*img->width],sizeof(struct pixel)*img->width,1,in);
        fseek(in, pad, SEEK_CUR);
    }
    
    if (count < img->height) {
        free(img->data);
        return READ_ERROR;
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    
    struct bmp_header header = create_header(img);
    
    const uint64_t height = img->height;
    const uint64_t width = img->width;
    struct pixel* data =  img->data;
    uint32_t const pad = padding(img->width);

    size_t count = 0;
    const uint8_t dump =0;
    
    if(!write_header(out, &header)) {free(img->data); return WRITE_HEADER_ERROR;}
    fseek(out, sizeof(struct bmp_header), SEEK_SET);


    for(size_t i = 0; i < height; i++){
        count = count + fwrite( &data[width*i], sizeof(struct pixel)*width, 1, out);
        if (fwrite( &dump, sizeof(uint8_t), pad, out) < pad) return WRITE_ERROR;
    }

    if(count < height){
        free(img->data);
        return WRITE_ERROR;
    }
    return WRITE_OK;
}


bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename) return false;    
    FILE* f = fopen( filename, "rb" ); 
    if (!f) return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true; 
    }

    fclose( f );
    return false;
}

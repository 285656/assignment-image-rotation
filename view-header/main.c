#include <stdbool.h>
#include <stdio.h>
#include <malloc.h>
#include "rotation.h"
#include "bmp.h"
#include "bmp_read_write.h"
#include "util.h"


static const char* array_read_status[] = {
    [READ_OK] = "ok",
    [READ_INVALID_HEADER] = "Invalid header bmp",
    [READ_NOT_NAME] = "Wrong file name",
    [READ_OPEN_ERR] = "READ Opening error",
    [READ_CLOSE_ERR] = "READ Closing error",
    [READ_ERROR] = "Reading error"
 };

static const char* array_write_status[] = {
    [WRITE_OK] = "ok",
    [WRITE_ERROR] = "error while writing",
    [WRITE_HEADER_ERROR] = "write header error",
    [WRITE_OPEN_ERR] = "WRITE Opening error",
    [WRITE_CLOSE_ERR] = "WROTE Closing error",    
};

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    struct bmp_header h = { 0 };
    
    if (read_header_from_file( argv[1], &h )) {
        bmp_header_print( &h, stdout );
    }
    else {
        err( "Failed to open BMP file or reading header.\n" );
    }
    
    
    //START
    struct image img = {0};
    
    enum read_status read_status = read_from_file_bmp( argv[1], &img );
    //printf(array_read_status[read_status]);
    if (read_status != READ_OK) {
        err(array_read_status[read_status]);
    }

    
    const char* name = "out.bmp";
    struct image rot_img = rotate_img(img, CLOCKWISE);
    enum write_status write_status= write_to_file_bmp(name, &rot_img);
   
    if (write_status != WRITE_OK) {
        err(array_write_status[write_status]);
    }
    
    
    free(img.data);
    free(rot_img.data);
    return 0;
}
